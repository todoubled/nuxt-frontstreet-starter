# nuxt-frontstreet-starter

![alt text](https://s3-us-west-2.amazonaws.com/themeblvd-projects/frontstreet/banner.png "Front Street Banner")

# Nuxt.js starter project with [ThemeBlvd/frontstreet](https://github.com/themeblvd/frontstreet)
You can follow the [framework instructions](https://github.com/themeblvd/frontstreet#method-2-full-framework) for including the dependencies based on your project.
FontAwesome icons are included in the /plugins/ path and added in the config.nuxt.js for convenience.

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

