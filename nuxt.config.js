module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'icreativepro',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Providing custom web design and development services' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Lora|Open+Sans|Teko', defer: true }
    ],
    script: [
/*    
** Add these if you need to include jQuery with a carousel / slider ** 
*/
      { src: '/js/jquery.min.js', defer: true },
      { src: '/js/owl.carousel.min.js', defer: true },
      { src: '/js/jquery.magnific-popup.min.js', defer: true },
      { src: '/js/frontstreet.min.js', defer: true }
    ]
  },
  css: [
    '@/assets/styles/main.scss',
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    },
    csp: false,
    dist: {
      // 1 year in production
      maxAge: '1y',
      // Don't serve index.html template
      index: false
    },
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7
    }
  },
  /*
  ** Generate an SPA fallback for dynamic routes : https://nuxtjs.org/guide/routing#spa-fallback
  */
  generate: {
    fallback: true
  },
  /*
  ** Customize the progress bar color - or - disable it
  */
  loading: false,
  /*
  ** Transitions
  */
  transition: {
    name: 'page',
    mode: 'out-in'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    analyze: false,
  },
  optimization: {
    chunks: 'all',
    automaticNameDelimiter: '.',
    name: undefined,
    cacheGroups: {}
  },
  styleResources: {
    scss: [
      './assets/styles/variables.scss',
      './assets/styles/mixins.scss'
    ]
  },
  plugins: [
    '~/plugins/global.js',
    '~/plugins/fontawesome.js',
    '~/plugins/lazyload.js'
  ],
  modules: [
    ['@nuxtjs/google-analytics', {
      id: process.env.NODE_ENV === 'production' ? 'UA-1551941-1' : 'UA-000000-1'
    }],
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ]
}

