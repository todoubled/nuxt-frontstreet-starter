import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false


//import icons needed
import { faCalendarAlt as CalendarAlt_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faCheck as Check_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faCircle as Circle_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faClock as Clock_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faCloud as Cloud_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faDesktop as Desktop_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faExternalLinkAlt as ExternalLinkAlt_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faExternalLinkSquareAlt as ExternalLinkSquareAlt_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope as Envelope_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faFolder as Folder_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faHome as Home_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faHeart as Heart_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faLongArrowAltLeft as LongArrowAltLeft_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faMapMarkerAlt as MapMarkerAlt_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faPaperPlane as PaperPlane_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faShare as Share_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faStar as Star_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faSyncAlt as SyncAlt_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faTags as Tags_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faTimes as Times_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faShoppingBasket as ShoppingBasket_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faUser as User_SolidIcon } from "@fortawesome/free-solid-svg-icons";
import { faWrench as Wrench_SolidIcon } from "@fortawesome/free-solid-svg-icons";


library.add(CalendarAlt_SolidIcon);
library.add(Check_SolidIcon);
library.add(Circle_SolidIcon);
library.add(Clock_SolidIcon);
library.add(Cloud_SolidIcon);
library.add(Desktop_SolidIcon);
library.add(Envelope_SolidIcon);
library.add(ExternalLinkAlt_SolidIcon);
library.add(ExternalLinkSquareAlt_SolidIcon);
library.add(Folder_SolidIcon);
library.add(Heart_SolidIcon);
library.add(Home_SolidIcon);
library.add(LongArrowAltLeft_SolidIcon);
library.add(MapMarkerAlt_SolidIcon);
library.add(PaperPlane_SolidIcon);
library.add(Share_SolidIcon);
library.add(Star_SolidIcon);
library.add(ShoppingBasket_SolidIcon);
library.add(SyncAlt_SolidIcon);
library.add(Tags_SolidIcon);
library.add(Times_SolidIcon);
library.add(User_SolidIcon);
library.add(Wrench_SolidIcon);

// Register the component globally
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)
